#!/usr/bin/env python
# -*- coding:gbk -*-
'''
@ author    :   zhao
@ file      :   fiction.py.py
@ version   :   garner
@ Date      :   2017/10/9 9:47
'''

import os
import re
import urllib2
import time

import shutil
from lxml import etree


def get_html(url):
    '''
    获取网页html内容
    :return: read
    '''
    url_obj = urllib2.Request(url)
    # 添加header头信息，浏览器伪装
    url_obj.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64) \
                                      AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36')
    try:
        res = urllib2.urlopen(url_obj)
    except urllib2.HTTPError:
        time.sleep(0.5)
        res = urllib2.urlopen(url_obj)
        print '\nurllib2.HTTPError! sleep 0.5s\n'
    html = res.read()
    res.close()
    # html = etree.HTML(html)
    return html


def get_xpath_html(url):
    '''
    将HTML转换成xpath格式
    :param url:
    :return:
    '''
    html = get_html(url)
    html = etree.HTML(html)
    return html


class Fiction(object):
    '''
    小说下载爬虫
    '''
    def __init__(self, url):
        self.chapter_url = url
        html = get_xpath_html(url)

        # 书名
        self.book_name = html.xpath('//h1/text()')[0].encode('gbk')

        # 第一章链接
        self.url = html.xpath('//dd/a/@href')[9]

    def get_chapter(self, new=''):
        # 每次下载时，清空文件内容
        with open('{}.txt'.format(self.book_name + new), 'w') as fic:
            fic.truncate()
        # 下载
        with open('{}.txt'.format(self.book_name + new), 'a') as fic:
            while self.url != self.chapter_url:
                html = get_xpath_html(self.url)

                # 章节名称
                chapter_name = html.xpath('//h1/text()')[0].encode('gbk')
                chapter_name = '{}\r\n\r\n'.format(chapter_name)

                # 章节内容
                chapter_content = html.xpath('//div[@id="content"]/text()')

                # 如果章节内容为空，则使用另一个路径（else里）
                if chapter_content != []:
                    pass
                else:
                    chapter_content = html.xpath('//div[@id="content"]/p/text()')

                print '正在下载{}\n'.format(chapter_name.strip())
                fic.write(chapter_name)
                for i in chapter_content:
                    fic.write('{}\r\n'.format(i.encode('gbk')))

                # 下一章链接
                self.url = html.xpath('//div[@class="bottem2"]/a/@href')[3]
            else:
                print '{}已下载至最新章节！'.format(self.book_name)

    def run(self, new=''):
        '''
        开始下载，从第一章开始
        '''
        if new == '':
            try:
                # 创建本书目录，并切换
                os.mkdir(self.book_name)
                os.chdir(self.book_name)
            except:
                # 已创建目录时，直接切换目录
                os.chdir(self.book_name)
            self.get_chapter()
            os.chdir('..')
        else:
            try:
                os.chdir(self.book_name)
            except:
                os.mkdir(self.book_name)
                os.chdir(self.book_name)
            self.get_chapter(new)
            os.chdir('..')

    def get_new(self, num=8):
        '''
        获取最新章节（默认下载最新9章，可自定义）
        :param num:（可自定义下载最新章节数）
        :return:
        '''
        html = get_xpath_html(self.chapter_url)
        # print self.chapter_url
        self.url = html.xpath('//dd/a/@href')[num]
        print self.url
        new = '_最新章节（{}）'.format(num+1)
        print '开始下载最新章节（最新{}章）'.format(num+1)
        self.run(new)

    def get_specify_chapter(self, name=None):
        html = get_html(self.chapter_url)
        reg = r'<dd><a href="(.*?)">(.*?){}(.*?)</a></dd>'.format(name)
        self.url = re.findall(reg, html)[0][0]
        new = '_{}_最新'.format(name)
        self.run(new)


# class Control(object):
#     def __init__(self):
#         self.search = 'http://www.biquge5200.com/modules/article/search.php?searchkey='
#
#
#     def view(self):
#         menu = '''\n请输入序号选择下载功能(0/1/2)：
#     1) 查看各类图书
#     2) 查询及下载图书
#     0)退出
#
#     请输入：'''
#         menu_sel = '请输入图书名称进行查询：\n'
#         menu_1 = '请选择图书序号：\n'
#         menu_2 = '是否开始下载？（Y/N）\n'




if __name__ == '__main__':

    url = 'http://www.biquge5200.com/60_60437/'
    fic = Fiction(url)
    # print fic.book_name
    # print fic.chapter_url
    # print fic.url
    # print '-*- ' * 20
    # fic.run()     # 开始下载（从头）
    fic.get_new()   # 开始下载（最新--默认最新9章）
    # fic.get_specify_chapter('君少爷的离间计')